#!/bin/bash

function run_query() {
	sql wikishared -- -e "$1"
}

language_list=`cat language_list.txt`

full_month=$1
if [ -z "$full_month" ]; then
	full_month=`date --date='-1 month' +%Y%m`
fi

year=${full_month:0:4}
month=${full_month:4:2}

if [ "$month" == "01" ]; then
	previous_month=$[year-1]12
else
	previous_month=$[full_month-1]
fi

echo "Running for $full_month"

for language in $language_list
do
	db_language=`echo $language | tr _ -`
	articles_during_the_previous_month_in_language=`run_query "select count(*) as article_count from cx_translations where (translation_status = 'published' or translation_target_url is not null) AND translation_last_updated_timestamp like '$previous_month%' AND translation_target_language='$language' order by article_count;" | grep "[0-9]"`
	articles_during_the_month_in_language=`run_query "select count(*) as article_count from cx_translations where (translation_status = 'published' or translation_target_url is not null) AND translation_last_updated_timestamp like '$full_month%' AND translation_target_language='$language' order by article_count;" | grep "[0-9]"`
	echo "Articles created (during the previous two months) in $language: $articles_during_the_previous_month_in_language $articles_during_the_month_in_language"
done

exit
