Every beginning of month, run these for the monthly report:

1. General monthly stats:

`./monthly_stats.sh`

This runs for about a minute.

2. Deletion statistics

`./count_deletion_range.sh FIRST_DAY LAST_DAY > FILENAME`

`FIRST_DAY` and `LAST_DAY` are timestamps, for example `20200801` and `20200831`.

`FILENAME` is a text file, for example `deletion-2020-08.txt`.

This may run for over an hour. It's recommended to run it in `screen`.

3. Two-month trend:

`./monthly_trend.sh > FILENAME`

`FILENAME` is a text file, for example `trend-2020-08.txt`.

This runs for about twenty minutes.  It's recommended to run it in `screen`.
