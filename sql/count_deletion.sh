#!/bin/bash
date=$1
if [ -z $date ]; then
	date=`date --date=yesterday +%Y%m%d`
fi

one_line=$2

echo "Deletions on $date"

query="SELECT count(distinct(ar_title)) FROM change_tag, archive WHERE ar_timestamp like '$date%' AND ar_namespace = 0 AND ct_tag_id = (select ctd_id from change_tag_def where ctd_name = 'contenttranslation') AND ar_rev_id = ct_rev_id ORDER BY NULL;"

total_deletions=0
language_list=`cat language_list.txt`

for language in $language_list
do
	db="${language}wiki"

	# Execute mysql
	result=`sql $db -- -e "$query" | grep "[0-9]"`

	if [ -z $one_line ]
	then
		if [ $result -ne 0 ]
		then
			printf "%-20s %d\n" $db $result
		fi
	else
		echo -n "$result	"
	fi

	((total_deletions = total_deletions + result))
done

echo "Total deletions for $date: $total_deletions"
