# wikimedia-language-team-scripts

Statistics scripts for the use by the Language team in the Wikimedia Foundation.

They are only for Wikimedia's statistics servers and probably won't make
a lot of sense elsewhere, at least not without significant modifications
for table names, server names, etc.

The `sql` directory is for scripts that read data from classic
MediaWiki SQL databases (as opposed to hive).
